import HiCtoolbox

import numpy
import os.path
import sys
import numpy as np
from numpy import linalg as LA
from sklearn.cluster import AgglomerativeClustering
import warnings


warnings.filterwarnings("ignore")


BASEDRESOLUTION=10000 #10kb
FINALRESOLUTION=10000 # has to be >=BASEDRESOLUTION, go from base to final

NUMBEROFCOMPPARTMENT=6
species='TAIR'
HICREPOSITORY='/home/carron/Comptestzone/smalltest/'
REFERENCEREPOSITORY='/home/carron/Comptestzone/geneandexon/exonDensity/10kb/'
REFERENCEGTF='/home/carron/Araport11_GFF3_genes_transposons.Mar92021.gff'
OUTREPOSITORY='/home/carron/Comptestzone/'

CHRSIZEFILE='/home/carron/generef/chromsize/tair10.chrom.sizes'
CENTROMEREFILE='/home/carron/generef/centrom/cemtromposthaliana-NULL.bed'

Sres=str(int(BASEDRESOLUTION/1000)) #if resolution is 10000, give '10' for '10kb'

how="Generatecomp-by-centromere"

#GENERATE COMMON BRICKS
#we need several value in order to generate the compartment
#usefull value to see for people
centromericdict=HiCtoolbox.LoadChrComplexCentromBeginandEnd(CENTROMEREFILE)
chrlist=HiCtoolbox.dictchr[species]

#GENE DENSITY IF NEEDED
if os.path.isfile(REFERENCEREPOSITORY+chrlist[0]+'.hdf5'):
	print('!!! Gene reference density is already present in ',REFERENCEREPOSITORY)
else:
	#fileofsexonssortbychr species chrsizefilename resolution nameout
	print("!!! Generate gene density from",REFERENCEGTF, ' in ' ,REFERENCEREPOSITORY)
	resolution=BASEDRESOLUTION
	chrsizedict=HiCtoolbox.loadchrsizedict(CHRSIZEFILE,resolution)
	avectordict=HiCtoolbox.makeemptydict(chrsizedict,chrlist)
	for i in chrlist:
		tosave=HiCtoolbox.makevecfromBED(REFERENCEGTF,avectordict,i,BASEDRESOLUTION)	
		print("!!! Generate gene density for chr",i)
		outname=REFERENCEREPOSITORY+i+".hdf5"
		HiCtoolbox.writehdf5(outname,tosave)


#FUNCTION of analysis

def makecompartment(chrmat,generef,numberofcomp):
	"""
	#INPUT :
	-HiC matrix of size s (chrmat)
	-Gene reference density of same size (generef)
	-Number of compartment you want in HMM (numberofcomp)
	#OUTPUT
	-Eigen value of the HiC matrix
	-Hidden markov states of the HiC matrix
	#Make all the operation :
	-filter the HiC matrix for low covered bin
	-generate the compartment
	-Orderate them (assign B comp to the lowest in gene density)
	-Return the compartement without filtering
	#Format of HMM compartment:
	-- -1 : seg ; 0 : Less gene, most inactive ; 1 : middle ; 2 : containt mostly gene
	HMM states are orderate by gene density from 0 to (numberofcomp-1)
	#NOTE
	-->This function does not take account of the resolution, it should be done before
	-->This function can be use to generate 2 comp by eigen value, that is orderate first by gene density
	"""
	[chrmat_FILTER,binsaved]=HiCtoolbox.filteramat(chrmat,1.5)
	generefseg=generef[binsaved] #FILTER the gene density with same filter as HiC
	chrmatlength=np.shape(chrmat)[0] # LENGTH before filtering
	chrmatlengthfilter=np.shape(chrmat_FILTER)[0] # LENGTH after filtering
	if chrmatlengthfilter>0:
		#PRE PROCESSING OF THE HI-C
		chrmat_FILTER=HiCtoolbox.SCN(chrmat_FILTER)
		chrmat_FILTER=HiCtoolbox.observed_expected(chrmat_FILTER)
		chrmat_FILTER=np.nan_to_num(chrmat_FILTER) #security
		corrmat=np.corrcoef(chrmat_FILTER)
		corrmat=np.nan_to_num(corrmat)
		eigenmat, eigenvalue = LA.eig(corrmat)
		#CLEANING THE MEMORY
		del eigenmat
		del chrmat_FILTER
		#Compartment by eigenvalue
		eigenvalue=eigenvalue[:,0] #first eigen vector
		eigenvalue=HiCtoolbox.checkcompartmentorientationoneigenvalue(eigenvalue,generefseg)
		#Compartment by HMM
		modelHMM,hidden_states=HiCtoolbox.makecompartimentbyGaussianHMM(corrmat,numberofcomp)
		hidden_states=HiCtoolbox.orderateHMMbyVP(hidden_states,eigenvalue)
		#PUT all filtered bin to -1
		hidden_statesUnfilter=HiCtoolbox.reversesegmentation1D(hidden_states,binsaved,chrmatlength)
		eigenvalueUnfilter=HiCtoolbox.reversesegmentation1D(eigenvalue,binsaved,chrmatlength)
	else:
		hidden_statesUnfilter=-1*np.ones(chrmatlength)
		eigenvalueUnfilter=-1*np.ones(chrmatlength)
	return eigenvalueUnfilter,hidden_statesUnfilter


def compbycentrom(ichr,baserep,centromericdict,resolution,repothersignal,repositoryout,NUMBEROFCOMP):
	"""
	#INPUT :
	int : index of chr to analyse(ichr), 
	str : repository of the data (baserep/)
	dict :centromere dict (centromericdict)
	int : resolution
	str : repository of gene density (repothersignal) 
	str : repository to store output
	int : number of compartment
	#OUTPUT :
	no output of the function, write ouput on outrep/
	"""
	INITIALRES=BASEDRESOLUTION
	print('==> Generation of compartment for chr ',ichr)
	chrmat=HiCtoolbox.loadhdf5(baserep+ichr+".hdf5")
	signalfile=HiCtoolbox.loadhdf5(repothersignal+ichr+".hdf5")
	if resolution>INITIALRES:
		print('==> Change of resolution from ',INITIALRES,' to ',resolution)
		chrmat=HiCtoolbox.binamatrixin2d(chrmat,INITIALRES,resolution)
		signalfile=HiCtoolbox.bin1D(signalfile,INITIALRES,resolution)
	if len(centromericdict[ichr])==1 and centromericdict[ichr][0][1]==0: 
		#NO CENTROMERE ON THAT CHROMOSOME
		print("==> No chunking by centromere on chr ",ichr)
		VP,compartment=makecompartment(chrmat,signalfile,NUMBEROFCOMP)
	elif len(centromericdict[ichr])==1: # ONE CENTROMERE ONLY
		#GENERATE CENTROMERE COORDINATE
		print("==> Chunking by centromere on chr ",ichr)
		poscentromresolutedbegin=int(np.ceil(centromericdict[ichr][0][0]/resolution))
		poscentromresolutedend=int(np.ceil(centromericdict[ichr][0][1]/resolution))
		centrompos=[poscentromresolutedbegin,poscentromresolutedend]
		print("position of centromere at that resolution : ",centrompos)
		#CHUNKING
		HiCbegin=chrmat[:poscentromresolutedbegin,:poscentromresolutedbegin]
		generefbegin=signalfile[:poscentromresolutedbegin]
		VPA,testpartA=makecompartment(HiCbegin,generefbegin,NUMBEROFCOMP)
		HiCend=chrmat[poscentromresolutedend:,poscentromresolutedend:]
		generefend=signalfile[poscentromresolutedend:]
		VPB,testpartB=makecompartment(HiCend,generefend,NUMBEROFCOMP)
		#AGREGATE chunck
		GAP1=np.ones(poscentromresolutedend-poscentromresolutedbegin)*-1
		compartment=np.concatenate((testpartA,GAP1,testpartB), axis=0)
		VP=np.concatenate((VPA,GAP1, VPB), axis=0)
	elif len(centromericdict[ichr])==2: # TWO CENTROMERE ON THAT CHROMOSOME
		print("==> Chunking by centromere on chr ",ichr)
		#GENERATE CENTROMERE COORDINATE
		pcentromresBegin1=int(np.ceil(centromericdict[ichr][0][0]/resolution))
		pcentromresEnd1=int(np.ceil(centromericdict[ichr][0][1]/resolution))
		pcentromresBegin2=int(np.ceil(centromericdict[ichr][1][0]/resolution))
		pcentromresEnd2=int(np.ceil(centromericdict[ichr][1][1]/resolution))
		#CHUNKING
		HiCbegin=chrmat[:pcentromresBegin1,:pcentromresBegin1]
		generefbegin=signalfile[:pcentromresBegin1]
		VPA,testpartA=makecompartment(HiCbegin,generefbegin,NUMBEROFCOMP)
		HiCmiddle=chrmat[pcentromresEnd1:pcentromresBegin2,pcentromresEnd1:pcentromresBegin2]
		generefmiddle=signalfile[pcentromresEnd1:pcentromresBegin2]
		VPB,testpartB=makecompartment(HiCmiddle,generefmiddle,NUMBEROFCOMP)
		HiCend=chrmat[pcentromresEnd2:,pcentromresEnd2:]
		generefend=signalfile[pcentromresEnd2:]
		VPC,testpartC=makecompartment(HiCend,generefend,NUMBEROFCOMP)
		#AGREGATE chunck
		GAP1=np.ones(pcentromresEnd1-pcentromresBegin1)*-1
		GAP2=np.ones(pcentromresEnd2-pcentromresBegin2)*-1
		compartment=np.concatenate((testpartA,GAP1,testpartB,GAP2,testpartC), axis=0)
		VP=np.concatenate((VPA,GAP1,VPB,GAP2,VPC), axis=0)
	#SAVE
	filenameout=repositoryout+ichr+"_compartiment.txt"
	#np.savetxt(filenameout,compartment)
	HiCtoolbox.writeasbed(compartment,ichr,FINALRESOLUTION,filenameout)
	filenameout=repositoryout+ichr+"_VP.txt"
	#np.savetxt(filenameout,VP)
	HiCtoolbox.writeasbed(VP,ichr,FINALRESOLUTION,filenameout)
	




#RUN

if __name__ == "__main__":
	if how=="Generatecomp-by-centromere":
		for achr in chrlist:
			compbycentrom(achr,HICREPOSITORY,centromericdict,FINALRESOLUTION,REFERENCEREPOSITORY,OUTREPOSITORY,NUMBEROFCOMPPARTMENT)
