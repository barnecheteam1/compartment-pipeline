# Compartment pipeline

The goal of this pipeline is to generate genomic compartment from Hi-C data.

CODE is human readable, now in procces to have more option

You should configure the begining of the main 
To make the pipeline work : python3 main.py

Option that is possible to had : a choice of HiC reader (HiCpro, rao, hdf5), actually it is hdf5

**INPUT data that you will need : **

_HI-C raw data :_ Actually stored in hdf5 format in the pipeline.

_Genomic background :_ In order to determine which component is A and which one is B you need a genomic background as a reference. Exons per base density or any active epigenomic marks should work in all species. To generate it I recomand to download a gff containing the gene annotation of the species :

_chromosome size :_ A tabulated file contaning all chromosome size of the studied genome is required. 
Should contain two collumns, the chromosome name and his size like this example for arabisopsis thaliana bellow.

1	30427671
2	19698289
3	23459830

_centromere position :_  A tabulated file containing chromosome, centromere begin and centromere end. Here an example on human.
chr1	121500000	128900000
chr2	90500000	96800000
chr3	87900000	93900000


**CONFIGURE THE main.py file **

In order to work, the pipeline need several value to be fill at the begining of the main _main.py_.

Here some help to configure it.

_BASEDRESOLUTION:_ Initial resolution of Hi-C raw and genomic background, for both (should be the same!).

_FINALRESOLUTION:_ Resolution you want the ouput result.

_OUTREPOSITORY:_ where you want the result.

_NUMBEROFCOMPPARTMENT:_ Do you want a 2 layer compartment or more? This integer define the number of compartment you want.

_species:_ species of your analysed data. Will be used by HiCtoolbox to have a list of chromosome to study. Actually in the pipeline : ['Human','Mouse','Drosophila','Chicken','MAIZE','TAIR']


**OUTPUT :**

Two tabulated file with the same format : Chromosome coordbinbegin coordbinend value

One containing the value of the eigenvalue. The other with integer compartment.
